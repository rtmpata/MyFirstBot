﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using MyFirstBot.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace MyFirstBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        //
        List<String> newsSources = new List<string>
        {
            "bbc-news", "cnn", "associated-press"
        };

        private async Task MessageReceivedAsync(IDialogContext context, 
            IAwaitable<IMessageActivity> result)
        {
            //var activity = await result as Activity;

            //// calculate something for us to return
            //int length = (activity.Text ?? string.Empty).Length;

            //// return our reply to the user
            //await context.PostAsync($"You sent {activity.Text} which was {length} characters");

            //context.Wait(MessageReceivedAsync);

            var message = await result;
            PromptDialog.Choice(
                context: context,
                resume: GetNewsAsync,
                options: newsSources,
                prompt: "Select your news source",
                retry: "Currently I support only BBC, CNN and AP. Please select one of them"
                );
            
        }

        private async Task GetNewsAsync(IDialogContext context, IAwaitable<String> result)
        {
            string newsSource = await result;
            var liveResult = GetNews(newsSource);
        }

        private async Task<LiveResponse> GetNews(string newsSource)
        {
            LiveResponse liveResponse = new LiveResponse();
            try
            {
                var client = new HttpClient();
                var response = await client.GetAsync(
                    string.Format("https://newsapi.org/v1/articles?source={0}&sortBy=top&apiKey=d61f401905b84a02a19a3752232e808c",
                    newsSource));
                var content = await response.Content.ReadAsStringAsync();
                liveResponse = JsonConvert.DeserializeObject<LiveResponse>(content);
                return liveResponse;
            }
            catch (Exception)
            {

                return liveResponse;
            }
        }
    }
}